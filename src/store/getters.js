import state from "./state.js"

export default {
    getTodoList(state){
        return state.todoList;
    }
}