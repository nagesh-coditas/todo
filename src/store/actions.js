import firebase from "firebase"
import router from '../main.js'
export default {

    async login({commit,state},payload){

        firebase.auth().signInWithEmailAndPassword(payload.email,payload.password).then((userCredential) => {
            var user = userCredential.user;
            state.uid = userCredential.user.uid;
            console.log("user logged Successfully",state.uid)
            state.isAuthenticated = true;
            router.replace('Todos')
          })
          .catch((error) => {
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log("error",errorMessage)
          });
          console.log("data",payload)
    },
    async register({commit,state},payload){
        firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then((userCredential) => {
        // Signed in 
        var user = userCredential.user;
        state.isAuthenticated = true;
        router.replace('Todos')
        console.log("user",user)
        // ...
        })
        .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        alert(errorMessage)
        console.log("error",errorMessage)
        // ..
        });

    },
    async logout({commit}){
        firebase.auth().signOut().then(() => {
            // Sign-out successful.
            router.replace('/')
          }).catch((error) => {
      });
    },
    async addTodo({commit},payload){

        let date = new Date();

        let Id = date.getTime();

        await firebase.database().ref('todos/' +Id).set({
        title: payload.title,
        content: payload.content,
        id:Id
        });
    },

    async updateTodo({commit},payload){

        // await firebase.database().ref('todos/' + payload.id).set({
        //     title:payload.title,
        //     content:payload.content,
        //     id:payload.id
        // })
        const response = await fetch(`https://vue-todo-2c687-default-rtdb.firebaseio.com/todos/` + `${payload.id}`+'.json',{
            method:'PUT',
            body:JSON.stringify({title:payload.title,content:payload.content,id:payload.id})
        })
        console.log("update",response)
        if(!response.ok){
            alert("error while updating todo")
        }else{
            alert("todo updated successfully")
        }

    },
    async deleteTodo({commit},payload){
        console.log("action delete todo",payload)

        try{
            const response = await  fetch(`https://vue-todo-2c687-default-rtdb.firebaseio.com/todos/` + `${payload}`+ '.json',{
                method:'DELETE'
            })

            console.log("output",response.message)
        }catch(e){
            console.log("delete error",e.message,e.error)
        }
    },
    async getTodos({commit}){

        try{

           const response =  await fetch(`https://vue-todo-2c687-default-rtdb.firebaseio.com/todos`+'.json')

           const output = await  response.json()

           commit('setTodos',output)

        }
        catch(e){
            console.log("get todos error",e.message)
        }
  
    }


}