import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import firebase from 'firebase'

var firebaseConfig = {
    apiKey: "AIzaSyBmmJrt0mCcjPpSZE1p7v4RCjcL6lfbeA8",
    authDomain: "vue-todo-2c687.firebaseapp.com",
    projectId: "vue-todo-2c687",
    storageBucket: "vue-todo-2c687.appspot.com",
    messagingSenderId: "60216055606",
    appId: "1:60216055606:web:8b1c0976d4ae976c6a0597"
  };
  firebase.initializeApp(firebaseConfig);


createApp(App).use(store).use(router).mount('#app')

export default router