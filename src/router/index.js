import { createRouter, createWebHistory } from 'vue-router'
import Signup from '../views/SignUp.vue'
import Todos from '../views/Todos.vue'
import PageNotFound from '../views/PageNotFound.vue'

const routes = [
  {

    path: "/:catchAll(.*)",
    component:PageNotFound
  },
  {
    path: '/',
    name: 'Signup',
    component: Signup,
  },
  {
    path: '/todos',
    name: 'Todos',
    component:Todos,
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
